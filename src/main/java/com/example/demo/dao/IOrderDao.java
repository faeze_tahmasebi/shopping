package com.example.demo.dao;

import com.example.demo.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface IOrderDao extends JpaRepository<Order, Integer>, QuerydslPredicateExecutor<Order> {

    Order findFirstByCustomerId(int c);


}
