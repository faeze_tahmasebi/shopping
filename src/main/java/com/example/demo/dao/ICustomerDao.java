package com.example.demo.dao;

import com.example.demo.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ICustomerDao extends JpaRepository<Customer,Integer> , QuerydslPredicateExecutor<Customer> {

}
