package com.example.demo.dao;

import com.example.demo.domain.Item;
import com.example.demo.domain.OrderDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface IItemDao extends JpaRepository<Item,Integer>, QuerydslPredicateExecutor<Item> {
    List<OrderDetail> findByItemId(Integer c, Pageable pageable);

}
