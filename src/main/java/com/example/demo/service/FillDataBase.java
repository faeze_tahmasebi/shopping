package com.example.demo.service;

import com.example.demo.dao.ICustomerDao;
import com.example.demo.dao.IItemDao;
import com.example.demo.dao.IOrderDao;
import com.example.demo.dao.OrderDetailDao;
import com.example.demo.domain.Customer;
import com.example.demo.domain.Item;
import com.example.demo.domain.Order;
import com.example.demo.domain.OrderDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class FillDataBase {
    @Autowired
    OrderDetailDao orderDetailDao;
    @Autowired
    ICustomerDao iCustomerDao;
    @Autowired
    IItemDao iItemDao;
    @Autowired
    IOrderDao iOrderDao;
   

    public OrderDetail saveItem() {
        Item item = new Item();
        item.setBrand("metal");
        item.setName("color");
        item.setPrice(500.0);
        iItemDao.save(item);

        Customer customer = new Customer();
        customer.setCustomerNmae("akbari");
        customer.setCustomerPhoneNumber("09124665102");
        iCustomerDao.save(customer);

        Order order = new Order();
        order.setCustomerId(customer.getCustomerId());
        order.setCustomer(customer);
        order.setTotalPrice(10000.0);
        iOrderDao.save(order);


        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setItem(item);
        orderDetail.setOrderId(order.getOrderId());
        orderDetail.setItemId(item.getItemId());
        orderDetail.setOrder(order);
        orderDetailDao.save(orderDetail);
        return orderDetail;

    }
}

