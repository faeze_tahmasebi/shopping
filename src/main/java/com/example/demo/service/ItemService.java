package com.example.demo.service;

import com.example.demo.dao.IItemDao;
import com.example.demo.dao.OrderDetailDao;
import com.example.demo.domain.*;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.EnumTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ItemService {
    @Autowired
    OrderDetailDao orderDetailDao;

    public List<OrderDetail> findById(Integer c) {
        Pageable page = PageRequest.of(0, 10);
        QOrderDetail qItem = QOrderDetail.orderDetail;
        BooleanExpression booleanExpression = qItem.orderId.eq(c);
        List<OrderDetail> orderDetailList = getList(orderDetailDao.findAll(booleanExpression, page));
        return orderDetailList;
    }

    public Set<Customer> findByItemId(Integer c) {
        Pageable page = PageRequest.of(0, 10);
        QOrderDetail qOrderDetail = QOrderDetail.orderDetail;
        BooleanExpression booleanExpression = qOrderDetail.itemId.eq(c);
        Set<Customer> customerSet = new HashSet<>();
        List<OrderDetail> orderDetailList = getList(orderDetailDao.findAll(booleanExpression, page));
        for (OrderDetail detailList : orderDetailList) {
            Order order = detailList.getOrder();
            Customer customer = order.getCustomer();
            customerSet.add(customer);
        }
        return customerSet;
    }

    public <E> List<E> getList(Iterable<E> iter) {
        List<E> list = new ArrayList();
        Iterator var3 = iter.iterator();
        while (var3.hasNext()) {
            E item = (E) var3.next();
            list.add(item);
        }

        return list;


    }


}
