package com.example.demo.service;

import com.example.demo.dao.IOrderDao;
import com.example.demo.domain.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    IOrderDao iOrderDao;

    public Order findbyid(int c) {
        Order order1 = iOrderDao.findFirstByCustomerId(c);
        return order1;

    }


}
