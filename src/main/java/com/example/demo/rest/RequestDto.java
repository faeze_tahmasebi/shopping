package com.example.demo.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Table;


public class RequestDto {

    @JsonProperty
    private String userName;
    @JsonProperty
    private String passWord;
}
