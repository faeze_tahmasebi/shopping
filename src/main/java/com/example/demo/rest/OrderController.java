package com.example.demo.rest;
//
import com.example.demo.config.SecurityConfiguration;
import com.example.demo.domain.*;

import com.example.demo.service.ItemService;
import com.example.demo.service.OrderService;
import com.example.demo.service.FillDataBase;
import lombok.extern.log4j.Log4j2;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.domain.Customer;

import java.awt.print.Book;
import java.util.List;
import java.util.Set;

@RestController
//@RequestMapping(value = "/shopping")
@Log4j2

public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    ItemService itemService;
    @Autowired
    FillDataBase fillDataBase;


    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/api/order", headers = "Accept=application/json;charset=UTF-8")
    public ResponseEntity<Order> findOrder(HttpServletRequest request, @RequestBody Customer customer) throws ServiceException {
        log.info("");
        return new ResponseEntity<Order>(orderService.findbyid(customer.getCustomerId()), HttpStatus.OK);
    }
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/boot/item/{orderid}", headers = "Accept=application/json;charset=UTF-8")
    public ResponseEntity<List<OrderDetail>> findOrderDetail(HttpServletRequest request, @PathVariable Integer orderid) throws ServiceException {
        log.info("");
        return new ResponseEntity<List<OrderDetail>> (itemService.findById(orderid), HttpStatus.OK);
    }
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/item/g/{itemid}", headers = "Accept=application/json;charset=UTF-8")
    public ResponseEntity<Set<Customer>> findSetCustomer(HttpServletRequest request, @PathVariable Integer itemid) throws ServiceException {
        log.info("");

        return new ResponseEntity<Set<Customer>> (itemService.findByItemId(itemid), HttpStatus.OK);
    }
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/item/saveitem", headers = "Accept=application/json;charset=UTF-8")
    public ResponseEntity<Item> saveItem(HttpServletRequest request,@RequestBody Item item) throws ServiceException {
        log.info("");
        return new ResponseEntity<Item> (fillDataBase.saveItem(), HttpStatus.OK);
    }
//    @ResponseStatus(HttpStatus.OK)
//    @PostMapping(value = "/item/saveCustomer", headers = "Accept=application/json;charset=UTF-8")
//    public ResponseEntity<Customer> saveCustomer(HttpServletRequest request,@RequestBody Customer customer) throws ServiceException {
//        log.info("");
//        return new ResponseEntity<Customer> (fillDataBase.saveCustomer(), HttpStatus.OK);
//    }
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/item/saveOrder", headers = "Accept=application/json;charset=UTF-8")
    public ResponseEntity<OrderDetail> saveOrder(HttpServletRequest request) throws ServiceException {
        log.info("");
        return new ResponseEntity<OrderDetail> (fillDataBase.saveItem(), HttpStatus.OK);
    }
//    @ResponseStatus(HttpStatus.OK)
//    @PostMapping(value = "/authenticate", headers = "Accept=application/json;charset=UTF-8")
//    public ResponseEntity<RequestDto> saveUser(HttpServletRequest request,  @RequestBody RequestDto requestDto) throws ServiceException {
//        log.info("");
//        return new ResponseEntity<RequestDto> (iRequestDao.save(requestDto), HttpStatus.OK);
//    }
//    mvn


    @GetMapping("/")
    public String greeting(Authentication authentication) {
        String userName = authentication.getName();
        return "Spring Security In-memory Authentication Example - Welcome " + userName;
    }
    }