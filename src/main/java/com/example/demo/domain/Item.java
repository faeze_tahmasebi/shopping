package com.example.demo.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "Item")
@AllArgsConstructor
@NoArgsConstructor
public class Item implements Serializable {

    private static final long serialVersionUID  = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer itemId;

    private String name;

    private Double price;

    private String brand;


//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
//    @JoinColumn(name = "orderDetailId", foreignKey = @ForeignKey(name = "FK_ORDERDetail_FREIGHTPLACE"))
//    List<OrderDetail> orderDetailList;

}
