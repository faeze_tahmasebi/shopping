package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "orderDetail")
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail implements Serializable {

    private static final long serialVersionUID  = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderDetailId;

    private Integer itemId;

    private Integer orderId;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "orderId", referencedColumnName = "orderId", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_ordre_orderdetail") )
    private Order order;


    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "itemId", referencedColumnName = "itemId", insertable = false, updatable = false , foreignKey = @ForeignKey(name = "fk_itm_orderdetail") )
    private Item item;
}


