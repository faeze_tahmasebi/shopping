package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "border")
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {

    private static final long serialVersionUID  = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderId;


    private Double totalPrice;

    private Integer customerId;

    @ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "customerId", referencedColumnName = "customerId" , insertable=false, updatable=false , foreignKey = @ForeignKey(name = "fk_costomer_order") )
    private Customer customer;

//    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
//    @JoinColumn(name = "orderDetailId", foreignKey = @ForeignKey(name = "FK_ORDERDetail_FREIGHTPLACE"))
//    List<OrderDetail> orderDetailList;



}
